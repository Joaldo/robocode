## Robocode

 O código é relativamente simples, ele baseia-se principalmente na posição dos oponentes para agir, ele busca um inimigo próximo ou com menos energia trava neste oponente até ele ser morto, contudo a prioridade de alvo pode ser alterado se um outro oponente atingi-lo. A movimentação é feita sempre buscando as bordas do campo de batalha nos ângulos de 90° principalmente, após encontrado, a movimentação sempre vai tender a ser ajustada para curvas em 90°, formando um quadrado. Outro ponto importante é que o dano e a energia gasta do robô vai variar de acordo com a distância do seu alvo atual, tendo menor dano/custo de energia em longas distâncias e o maior dano/custo de energia ao se aproximar do alvo, tendo em vista que, a probabilidade de errar o alvo é menor em  distâncias menores. 

 Os pontos fortes são a movimentação que é feita de forma constante, diminuindo a chance de ser atingido por oponentes e a adaptabilidade do dano/custo de energia em relação a distância dos oponentes visados. 

 Entre os pontos fracos estão a necessidade de disparos consecutivos, visto que a implementação não possui uma predição de movimento, atrelado a isso, está a dificuldade em acertar alvos em constante movimento, tendo em vista que o programa vai verificar a posição do oponente e disparar e esse disparo leva um certo tempo até chegar na posição do alvo.

 Uma das surpresas positivas da implementação foi ter descoberto classes e utilidades "diferentes" para a linguagem Java, já tenho alguma experiência com a linguagem Java e não fazia ideia que também poderia usá-la para essa finalidade, ter descoberto essas classes e métodos e poder transformá-los em um robô de batalha foi uma experiência diferente, incrível e empolgante.
