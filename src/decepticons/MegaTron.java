package decepticons;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
 
public class MegaTron extends AdvancedRobot{

    private boolean movimento = false; 
    private boolean naParede = false; 
    private String alvo;
    private byte giro = 0; 
    private byte direcao = 1; 
    private short energia; 

    @Override
    public void run(){
        setColors(Color.BLACK, Color.DARK_GRAY, Color.BLACK); 
        setAdjustGunForRobotTurn(true); // ajustar arma quando o robo girar
        setAdjustRadarForGunTurn(true); // ajustar o radar quando o robo girar
        while(true){
            turnRadarLeftRadians(2); //sentido do radar
        }
    }

    @Override
    public void onHitByBullet(HitByBulletEvent e){
        alvo = e.getName(); // travar no robo que atirou em mim
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e){
        if(alvo == null || giro > 6){ 
            alvo = e.getName(); // se nao possui alvo travado, escolha o proximo
        }

        if(getDistanceRemaining() == 0 && getTurnRemaining() == 0){
            if(naParede){
                if(movimento){
                    setTurnLeft(90); // mover em circulo
                    movimento = false;
                }
                else{ 
                    setAhead(270 * direcao); // se move em "circulo" na direcao oposta
                    movimento = true; 
                }
            }
            else{ // ficar proximos ao paredes sem colar nelas
                if((getHeading() % 90) != 0){
                    setTurnLeft((getY() > (getBattleFieldHeight() / 2)) ? getHeading()
                            : getHeading() - 180);
                }
                // se estiver longe das paredes de cima e baixo
                // va para proximo as paredes
                else if(getY() > 30 && getY() < getBattleFieldHeight() - 30){
                    setAhead(getHeading() > 90 ? getY() - 20 : getBattleFieldHeight() - getY()
                            - 20);
                }
                // se estiver longe das paredes laterais
                // va para proximo as paredes
                else if(getHeading() != 90 && getHeading() != 270){
                    if(getX() < 350){
                        setTurnLeft(getY() > 300 ? 90 : -90);
                    }
                    else{
                        setTurnLeft(getY() > 300? -90 : 90);
                    }
                }
                // se nao estiver longe das paredes laterais, vai se aproximar
                else if(getX() > 30 && getX() < getBattleFieldWidth() - 30){
                    setAhead(getHeading() < 180 ? getX() - 20 : getBattleFieldWidth() - getX()
                            - 20);
                }
                // se estiver nos cantos, vira e comeca a anda
                else if(getHeading() == 270){
                    setTurnLeft(getY() > 200 ? 90 : 180);
                    naParede = true;
                }
                // se estiver nos cantos, vira e comeca a anda
                else if(getHeading() == 90){
                    setTurnLeft(getY() > 200 ? 180 : 90);
                    naParede = true;
                }
            }
        }
        if(e.getName().equals(alvo)){ // se o alvo travado for morto
            giro = 0; // reseta o contador do radar

            if((energia < (energia = (short)e.getEnergy())) && Math.random() > .85){ 
                direcao *= -1; 
            }

            setTurnGunRightRadians(Utils.normalRelativeAngle((getHeadingRadians() + e
                    .getBearingRadians()) - getGunHeadingRadians())); // mirar na direcao do oponente 

            if(e.getDistance() < 100){ // se o inimigo estiver a menos que 100px atire com 5
		setFire(5.0); 
            }else if(e.getDistance() < 200) {
            	setFire(2.5);
            }else{
                setFire(1.5); 
            }
        }
        else if(alvo != null){ // adicione um giro ao contador
            giro++;
        }
    }

}